package bicca.lucas.theatertickets.home.view;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import bicca.lucas.theatertickets.R;
import bicca.lucas.theatertickets.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.activityMainDateEditText.setOnFocusChangeListener(this);

    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus && isDateFocused(view)) {
            showDatePicker();
        }
    }

    private void showDatePicker() {
        binding.activityMainDatePicker.setVisibility(View.VISIBLE);
        binding.activityMainDateEditText.clearFocus();
        binding.activityMainDatePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        binding.activityMainCalculateButton.setVisibility(View.GONE);
    }

    private boolean isDateFocused(View view) {
        return view.getId() == binding.activityMainDateEditText.getId();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }
}
