package bicca.lucas.theatertickets.home.model;

import java.util.Calendar;
import java.util.Date;

import bicca.lucas.theatertickets.home.util.DateUtil;

public class Calculate {

    private static final double CHILD_PRICE = 5.50;
    private static final double STUDENT_PRICE = 8.00;
    private static final double ELDERLY_PRICE = 6.00;

    public static double perform(User user) {
        double price = 0;
        int discount = -1;
        int dayOfWeek = DateUtil.getDayOfWeek(user.getDate());
        switch (user.getUserType()) {
            case CHILD:
                price = CHILD_PRICE;
                discount = getChildDiscount(dayOfWeek);
                break;
            case STUDENT:
                price = STUDENT_PRICE;
                discount = getStudentDiscount(dayOfWeek, user.hasId(), user.getDate());
                break;
            case ELDERLY:
                price = ELDERLY_PRICE;
                discount = getElderlyDiscount(dayOfWeek);
                break;
        }
        return perform(discount, price);
    }

    public static double perform(int discount, double normalPrice) {
        double discountPrice;
        discountPrice = (normalPrice * discount) / 100;
        discountPrice = normalPrice - discountPrice;
        return discountPrice;
    }

    public static int getChildDiscount(int dayOfWeek) {
        int discount;
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                discount = 10;
                break;
            case Calendar.TUESDAY:
                discount = 15;
                break;
            case Calendar.WEDNESDAY:
                discount = 30;
                break;
            case Calendar.FRIDAY:
                discount = 11;
                break;
            default:
                discount = 0;
        }
        return discount;
    }

    public static int getStudentDiscount(int dayOfWeek, boolean hasId, Date date) {
        if (hasId && !DateUtil.isWeekend(date)) {
            return 35;
        }
        int discount;
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                discount = 10;
                break;
            case Calendar.TUESDAY:
                discount = 5;
                break;
            case Calendar.WEDNESDAY:
                discount = 50;
                break;
            case Calendar.THURSDAY:
                discount = 30;
                break;
            default:
                discount = 0;
        }
        return discount;
    }

    public static int getElderlyDiscount(int dayOfWeek) {
        int discount;
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                discount = 10;
                break;
            case Calendar.TUESDAY:
                discount = 15;
                break;
            case Calendar.WEDNESDAY:
                discount = 40;
                break;
            case Calendar.THURSDAY:
                discount = 30;
                break;
            default:
                discount = 0;
        }
        return discount;
    }
}
