package bicca.lucas.theatertickets.home.model;

public enum UserType {
    CHILD, STUDENT, ELDERLY
}
