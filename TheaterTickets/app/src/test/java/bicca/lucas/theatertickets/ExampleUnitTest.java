package bicca.lucas.theatertickets;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import bicca.lucas.theatertickets.home.model.Calculate;
import bicca.lucas.theatertickets.home.model.User;
import bicca.lucas.theatertickets.home.model.UserType;
import bicca.lucas.theatertickets.home.util.DateUtil;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private Date getDate(int dayOfWeek) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return calendar.getTime();
    }

    @Test
    public void getDayOfWeek() throws Exception {
        Date date = getDate(Calendar.SUNDAY);
        int day = DateUtil.getDayOfWeek(date);
        assertEquals(Calendar.THURSDAY, day);
    }

    @Test
    public void getWeekend() throws Exception {
        Date date = getDate(Calendar.SATURDAY);
        boolean isWeekend = DateUtil.isWeekend(date);
        assertEquals(true, isWeekend);
    }

    @Test
    public void getDiscount() throws Exception {
        double price = Calculate.perform(30, 6.50);
        assertEquals(4.50, price);
    }

    @Test
    public void getElderlyDiscount() throws Exception {
        User user = new User();
        user.setDate(new Date());
        user.setUserType(UserType.ELDERLY);
        double price = Calculate.perform(user);
        assertEquals(4.20, price, 0);
    }

    @Test
    public void getChildDiscount() throws Exception {
        User user = new User();
        user.setDate(new Date());
        user.setUserType(UserType.CHILD);
        double price = Calculate.perform(user);
        assertEquals(5.5, price, 0);
    }

    @Test
    public void getStudentDiscountWithId() throws Exception {
        User user = new User();
        user.setDate(new Date());
        user.setUserType(UserType.STUDENT);
        user.setHasId(true);
        double price = Calculate.perform(user);
        assertEquals(5.2, price, 0);
    }

    @Test
    public void getStudentDiscount() throws Exception {
        User user = new User();
        user.setDate(new Date());
        user.setUserType(UserType.STUDENT);
        user.setHasId(false);
        double price = Calculate.perform(user);
        assertEquals(5.6, price, 0);
    }
}